import { resolve } from "path";
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import Components from "unplugin-vue-components/vite";
import { AntDesignVueResolver } from "unplugin-vue-components/resolvers";
// import usePluginImport from 'vite-plugin-importer';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    Components({
      resolvers: [
        AntDesignVueResolver({ importStyle: "less", resolveIcons: true }),
      ],
    }),
    // usePluginImport({
    //   libraryName: 'ant-design-vue',
    //   customName: (name) => {
    //     return `ant-design-vue/es/${name}`;
    //   },
    // })
  ],
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src') // 兼容src目录下的文件夹可通过 @/components/HelloWorld.vue写法 
    }
  },

  // 在.vue文件增加<style scoped lang="scss">即可
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: ['@import "./src/style/base.scss";'], // 全局公共样式
        charset: false,
        // The legacy JS API is deprecated and will be removed in Dart Sass 2.0.0
        // Vite仍然默认使用传统的API，但您可以通过将api设置为"modern"或"modern-compiler"来类似地切换它
        // 还不能换，会报错(还没升级到2，现在只是提醒)
        // api: 'modern-compiler'
      }
    }
  }
})
