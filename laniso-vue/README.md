# Vue 3 + TypeScript + Vite

This template should help get you started developing with Vue 3 and TypeScript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

Learn more about the recommended Project Setup and IDE Support in the [Vue Docs TypeScript Guide](https://vuejs.org/guide/typescript/overview.html#project-setup).


## 文献
[项目搭建](https://blog.csdn.net/qq_35221977/article/details/137171497)  
[Node:npm:版本](https://nodejs.org/en/about/previous-releases)  
[Node:环境搭建](https://blog.csdn.net/WHF__/article/details/129362462)  
[sass:less](https://zhuanlan.zhihu.com/p/687150036)  
[Warn:The legacy JS API is deprecated](https://blog.csdn.net/CssHero/article/details/142686148)  
[Electron](https://zhuanlan.zhihu.com/p/582050432)  
[ElementPlus:Ant-Design:Quasar](https://www.zhihu.com/question/464055499/answers/updated)  
[PrimeVue](https://diamond.primevue.org/)  
[自定义主题色](https://www.jianshu.com/p/9d1df66df645)  
[vue:按需加载:1](https://blog.csdn.net/qq_52229892/article/details/130101735)  
[vue:按需加载:2](https://blog.csdn.net/harmsworth2016/article/details/117407251)  
[vue:按需方案](https://blog.csdn.net/CSND7997/article/details/118083608)  
[ant-design:4.2.5](https://www.antdv.com/components/layout-cn)

## Tips
npm install --save xxx (可以同时将配置写入 package.json 的 dependencies 字段)
npm install xxx -D (可以同时将配置写入 package.json 的 devDependencies 字段)
