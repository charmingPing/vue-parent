import { createApp } from 'vue'
import App from './App.vue'
import router from './router' // 注册路由 
import register from './base'

const app = createApp(App)
register(app)
app.use(router).mount('#app')
