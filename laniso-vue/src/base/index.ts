import { Button, Input, TimePicker } from "ant-design-vue";
import { Modal, message } from 'ant-design-vue';

// 通用导入，有需要的就在这边加
const components = [Button, Input, TimePicker]
export default function register(app: any) {
    components.forEach((component) => {
        app.use(component)
    })
    // 挂载全局对象
    app.provide('message', message)
    app.provide('modal', Modal)
}
