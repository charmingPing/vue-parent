import { defineStore } from "pinia";
 
export const store = defineStore('counter', {
    state: () => ({
        count: 0
    }),
    actions: {
        increment() {
            this.count++;
        },
        incrementAsync() {
            setTimeout(() => { this.increment() }, 1000);
        }
    }
});