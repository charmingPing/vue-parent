# demo-vue

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### 调防技巧
1. 悬浮显示、下拉显示的元素需要调试的，选中元素右击 -> 开发者工具


### 在线工具
- [ ] http://www.ico51.cn/


### 参考资料
1. [vue使用ant-1.7.8](https://1x.antdv.com/docs/vue/introduce-cn/)
2. 使用 babel-plugin-import
3. 升级vue版本2.5 升级至2.7
4. 查看vue-cli版本
5. @vue/cli 4.5.18
6. vue cli 4.5.18 创建项目
7. babel-plugin-import 没有  .babelrc
8. error  'DatePicker' is defined but never used 
9. [ES6 赋值写法](https://blog.csdn.net/qq_40028324/article/details/80961567)
10. [Vue Router](https://www.cnblogs.com/wenpeng/p/12362734.html)
11. [Ant Vue Router](https://blog.csdn.net/yjh4866/article/details/121964335)
12. [Ant 主题切换](https://blog.csdn.net/qq873113580/article/details/123008042)
13. [平滑过渡](https://blog.csdn.net/weixin_43180359/article/details/106278987)
14. [css伪类实现点击](https://blog.csdn.net/banglianqian3400/article/details/101627378)
15. [伪类事件](https://blog.csdn.net/leng0920/article/details/77413042)
16. [子元素选择父元素](https://blog.csdn.net/qq_20343517/article/details/82990211)
17. [div,span,a,li获取focus事件](https://blog.csdn.net/WuLex/article/details/104544432)
18. [传参问题](https://blog.csdn.net/SmallTeddy/article/details/107204915)
19. [导航栏吸住](https://blog.csdn.net/webKris/article/details/85631098)
20. [路由不嵌套，在浏览器进行了跳转](https://blog.csdn.net/ken11253/article/details/119223490)
21. [静态资源src无效](https://blog.csdn.net/weixin_44817017/article/details/114526291)
22. [组件、变量未使用](https://blog.csdn.net/huzhenv5/article/details/104015182)
23. [警告：Named Route 'Home' has a default child route](https://www.cnblogs.com/zhixi/p/9640785.html)
24. [Tabs 标签页](https://blog.csdn.net/Principal_Z/article/details/119078369)
25. [css 动画](https://blog.csdn.net/happyhaojie/article/details/50733839)
26. [Befor, After](https://blog.csdn.net/weixin_39911007/article/details/119371012)
27. [元素拖拽](https://blog.csdn.net/m0_61342618/article/details/127360847)
28. [自定义标签](https://juejin.cn/post/6844903833475219463)
29. [边框阴影](https://blog.csdn.net/qq_43353619/article/details/114580409)
30. [CSS 模拟鼠标点击事件](https://www.bbsmax.com/A/n2d91jOYdD/)
- [ ] https://blog.csdn.net/m0_56219678/article/details/123089683
- [ ] https://juejin.cn/post/7051206427402043423
- [ ] https://blog.csdn.net/weixin_49993071/article/details/124371467
- [ ] https://blog.csdn.net/qq_41809113/article/details/125571266
- [ ] https://blog.csdn.net/xiaowu0929/article/details/126624336
- [ ] https://yadong.blog.csdn.net/article/details/126840777
- [ ] https://blog.csdn.net/weixin_39788999/article/details/105022415
- [ ] https://blog.csdn.net/weixin_41387351/article/details/120654805
- [ ] https://www.cnblogs.com/gujun1998/p/13659430.html
- [ ] https://blog.csdn.net/weixin_43334673/article/details/109171140
- [ ] https://blog.csdn.net/qq_33189961/article/details/107165910
- [全局滑块样式](https://blog.csdn.net/qq_35269556/article/details/123090700)
- [隐藏滚动条](https://www.cnblogs.com/xiaoyaoxingchen/p/9147824.html)
- [全局Api封装 - 1](https://blog.csdn.net/weixin_44278376/article/details/113860367)
- [全局Api封装 - 2](https://www.jianshu.com/p/90e1d594638a)
- [请求代理](https://juejin.cn/post/7049555869024911367)
- [SCSS 覆盖样式](https://blog.csdn.net/weixin_45108907/article/details/108193884)
- [vue在sass或css中使用变量，动态改变样式](https://blog.csdn.net/qq_45327886/article/details/126022897)
- [css中使用js的变量](https://juejin.cn/post/7005744294799605773)

#### 父子组件传参
https://blog.csdn.net/denghuocc/article/details/111040726
https://juejin.cn/post/7005744294799605773
https://blog.csdn.net/weixin_44901846/article/details/107499063

#### 在线阴影生成器
https://lingdaima.com/shadow/
#### collapsedWidth设置与menu不匹配问题
无法解决
https://github.com/NG-ZORRO/ng-zorro-antd/issues/6652
https://github.com/ant-design/pro-components/issues/122
https://github.com/ant-design/ant-design-pro/issues/3748
### 跟随下划线
https://www.jianshu.com/p/90122dc37b45
https://juejin.cn/post/7149188833421033485
https://blog.csdn.net/qq_43156398/article/details/106081489
### 监听动画
https://blog.csdn.net/lingfengjgf/article/details/106637922
https://www.cnblogs.com/afei-qwerty/p/6869023.html
https://blog.csdn.net/weixin_45281192/article/details/102903028
https://blog.csdn.net/qq_38128179/article/details/118934676
https://blog.csdn.net/qq_39335404/article/details/129339539
https://segmentfault.com/a/1190000038459089
### transform
https://www.coder.work/article/1124936
https://developer.mozilla.org/zh-CN/docs/Web/CSS/transform
### 添加less
yarn add less less-loader@5 --save
yarn remove less less-loader@5
### 图标库
https://www.aigei.com/
https://www.butterpig.top/icopro
### delete参数无法识别
https://blog.csdn.net/haodian666/article/details/134639532
https://segmentfault.com/q/1010000040730969
https://blog.csdn.net/qq_35697034/article/details/100558206
https://huaweicloud.csdn.net/63876fcfdacf622b8df8c3ab.html
### css子元素选择器
https://segmentfault.com/a/1190000010359642
https://blog.csdn.net/weixin_43221323/article/details/123373707
### 安装typescript
yarn global add typescript
https://juejin.cn/post/7174610946713714702
https://blog.csdn.net/xtho62/article/details/108867555
### 链接kafka
https://huaweicloud.csdn.net/637f783edacf622b8df85167.html
https://juejin.cn/post/6844903904203784199
### 文件导出
https://zhuanlan.zhihu.com/p/474969648
### 日期格式化
https://blog.csdn.net/XG17_38241417145/article/details/120000551
### 疑问
1. 布局
```
<div class="header-right">
    <ul class="header-right-ul">
    <li tabindex="0">
        <a-icon type="search" />
    </li>
    <li tabindex="1">
        <a-icon type="question-circle" />
    </li>
    <li tabindex="2">
        <a-popover title="最新消息" placement="bottomRight" :trigger="['click']">
        <!-- 为什么要用template？ -->
        <template slot="content">
            <p>Content</p>
            <p>Content</p>
        </template>
        <a-badge dot>
            <a-icon type="bell" />
        </a-badge>
        </a-popover>
    </li>
    <li tabindex="3">
        <!-- 默认移入触发，设置为点击触， :placement 报未定义？-->
        <a-dropdown class="user" placement="bottomRight" :trigger="['click']" overlayClassName="user-drop-root">
        <a class="ant-dropdown-link" @click="e => e.preventDefault()">
            <!-- 手写头像 -->
            <!-- <img class="avatar" src="@/images/avatar_1.png" /> -->
            <!-- 框架自带头像 -->
            <a-avatar class="avatar">A</a-avatar>
            admin
        </a>
        <a-menu slot="overlay">
            <a-menu-item key="1">
            <a href="javascript:;">
                <a-icon type="user" /> 个人中心
            </a>
            </a-menu-item>
            <a-menu-divider />
            <a-menu-item key="2">
            <a href="javascript:;">
                <a-icon type="logout" /> 退出登录
            </a>
            </a-menu-item>
        </a-menu>
        </a-dropdown>
    </li>

    <!-- <a-popover title="最新消息" placement="bottomRight" :trigger="['click']">
        <template slot="content">
        <p>Content</p>
        <p>Content</p>
        </template>
        <li tabindex="2">
        <a-badge dot>
            <a-icon type="bell" />
        </a-badge>
        </li>
    </a-popover> -->
    
    </ul>
</div>
```
2. Vue知识
    - [ref](https://blog.csdn.net/weixin_38981993/article/details/82944629)
    - [属性](https://blog.csdn.net/weixin_51351637/article/details/126764326)
    - [自定义组件](https://blog.csdn.net/qq_39611230/article/details/106957664)
    - [watch](https://blog.csdn.net/Guzarish/article/details/118625559)
    - [路由传参数1](https://blog.csdn.net/qq_38543537/article/details/80350107)
    - [路由传参数2](https://blog.csdn.net/cdgogo/article/details/107868220)
    - [ref](https://blog.csdn.net/m0_61612505/article/details/124365060)
    - [this.$refs打印为undefined](https://blog.csdn.net/changzhen11/article/details/84067816)
    - [vue watch中获取this.$refs.xxx节点的方法](https://blog.csdn.net/hjh15827475896/article/details/121927971)
    - [Vue2 条件渲染和循环渲染](https://blog.csdn.net/qq_56607109/article/details/126994874)
    - [Elements in iteration expect to have ‘v-bind:key‘ directives.‘](https://blog.csdn.net/qq_43540219/article/details/107615142)
    - [报错‘＜template v-for＞‘ cannot be keyed.](https://blog.csdn.net/The_more_more/article/details/124655568)
    - [error Unexpected ‘debugger‘ statement no-debugger](https://blog.csdn.net/mochen_mj/article/details/108537446)
    - [vue click点击事件传入事件对象、自定义参数](https://blog.csdn.net/weixin_44706267/article/details/122073650)
    - [.self](https://blog.csdn.net/XuM222222/article/details/80276884)
    - [vue中阻止冒泡事件](https://blog.csdn.net/qq_40666120/article/details/106071521)
    - [vue中动态添加class类名](https://blog.csdn.net/qq940853667/article/details/77413323)
    - [v-for v-if](https://blog.csdn.net/namechenfl/article/details/83987488)
    - [vue 动态类名](https://blog.csdn.net/u013243347/article/details/88557992)
    - [CSS](https://chokcoco.github.io/#blog)
    - [Vue 动画过渡](https://blog.csdn.net/EX1332914934/article/details/124686920)
    - [Vue 动画过渡](https://blog.csdn.net/Lushengshi/article/details/126517661)
    - [Vue 动画过渡](https://blog.csdn.net/purple_lumpy/article/details/83241965)
    - [vue router >=3 重复点击报错](https://blog.csdn.net/Vest_er/article/details/127094210)
    - [NavigationDuplicated: Avoided redundant navigation to current location](https://blog.csdn.net/qq_29252021/article/details/109615753)
    - [vue子组件怎么向父组件传值](https://www.php.cn/vuejs/480569.html)
    - [vue 自定义右键菜单方式](https://blog.csdn.net/qq_38143787/article/details/107002061)
    - [ant menu](https://segmentfault.com/q/1010000016605457/a-1020000016606209)
    - [vue props中定义多属性对象](https://blog.csdn.net/wsjzzcbq/article/details/120851968)
    - [内存泄露问题](https://blog.csdn.net/qq_44732146/article/details/130026619)
    - [vue:destroy](https://blog.csdn.net/weixin_47148731/article/details/123658984)
    - [vue:destroy](https://v2.cn.vuejs.org/v2/cookbook/avoiding-memory-leaks.html)
    - [vue:destroy](https://blog.csdn.net/qq_42740797/article/details/120451600)
    - [vue:升级](https://blog.csdn.net/wangshuai6707/article/details/132048387)
    - [环境搭建](https://blog.csdn.net/weixin_69553582/article/details/129584587)