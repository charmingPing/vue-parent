/**
 * https://www.jianshu.com/p/142f2231355e
 * https://blog.csdn.net/CuiCui_web/article/details/107042226
 */
const mysql = require('mysql');  // 引入数据库
var pool = mysql.createPool({      //创建mysql实例
  host:'192.168.2.34', //localhost/192.168.2.34
  port:'3306',
  user:'root',
  password:'123456', // root/123456
  database:'electric'  // test/electric
 });

 pool.getConnection(function(err, connection) { // 使用连接池
  if (err) {
    console.log('mysql connect err！');
  } else {
    console.log('mysql is connected.');
  }
});

// 接收一个sql语句 以及所需的values
// 这里接收第二参数values的原因是可以使用mysql的占位符 '?'
// 比如 query(`select * from my_database where id = ?`, [1])
let query = function(sql, values) {
  // 返回一个 Promise
  return new Promise((resolve, reject) => {
    pool.getConnection(function(err, connection) {
      if (err) {
        reject(err)
      } else {
        connection.query(sql, values, (err, rows) => {
          if (err) {
            reject(err)
          } else {
            resolve(rows)
          }
          // 结束会话
          connection.release()
        })
      }
    })
  })
}

module.exports = query
