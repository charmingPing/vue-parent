const Mock = require('mockjs');
const moment = require('moment');

const bmsa = {
    "MessageType": 604,
    "DeviceType": 0,
    "CcmSn": "21000SEA12800778",
    "Time": "2023-1-6 13:27:55",
    "Time_uint64": 1673011675,
    "CreationDate":"2024-01-17 18:34:00",
    "TotalCluster": 8,
    "Data": [
        {
            "Sn": "cluster_4050S0100",
            "No": 1,
            "ComStatus": 1,
            "Type": "Cluster",
            "Name": "簇别名",
            "ParamData": {
                "sys_rtc_year_month": "0",
                "sys_rtc_day_hour": "0",
                "sys_rtc_minute_second": "0",
                "SI_DTS_SysModeSts": "3",
                "SI_BSW_AfeComSts": "0",
                "SI_BSW_SlaveMcuComSts": "0",
                "SI_BSW_PackLinkVolt": "1391.5",
                "SI_BSW_AfterBreakLinkVolt": "0",
                "SI_BSW_OutputLinkVolt": "0",
                "SI_BSW_PackCurrDir": "2",
                "SI_BSW_PackCurrA": "110.8",
                "SI_BSW_PackOutputPower": "0",
                "SI_DTS_DisplaySOC": "99.4",
                "SI_DTS_DisplaySOH": "100",
                "SI_BSW_BatCtrlStatusMsg": "0",
                "SI_DTS_PRly_DiscMosSts": "1",
                "SI_DTS_NRly_ChgMosSts": "1",
                "SI_DTS_LmtChgMduSts": "0",
                "SI_DTS_PreChgRlySts": "1",
                "SI_DTS_BatChargeVoltUpper": "1518.4",
                "SI_DTS_BatDischgVoltLower": "1206.4",
                "SI_DTS_BatAllowChargeCurrMax": "170",
                "SI_DTS_BatAllowDischgCurrMax": "170",
                "SI_DTS_BatForceChargeEnable": "0",
                "EI_BSW_SafetyStandardCode": "0",
                "SI_BSW_BCUEnviTemp": "23.8",
                "SI_BSW_MPosRly_DiscMosTemp": "28.0",
                "SI_BSW_MNegRly_ChgMosTemp": "27.8",
                "SI_BSW_CurrSampResTemp": "29.0",
                "SA_DTS_CellVoltMaxTop5": [
                    "3540",
                    "3536"
                ],
                "SA_DTS_CellVoltMaxTop5ID": [
                    "144",
                    "141"
                ],
                "SA_DTS_CellVoltMinTop5": [
                    "3486",
                    "3484"
                ],
                "SA_DTS_CellVoltMinTop5ID": [
                    "250",
                    "248"
                ],
                "SI_DTS_CellVoltMaxDiff": "45",
                "SA_DTS_CellTempMaxTop5": [
                    "28",
                    "26"
                ],
                "SA_DTS_CellTempMaxTop5ID": [
                    "187",
                    "186"
                ],
                "SA_DTS_CellTempMinTop5": [
                    "26",
                    "26"
                ],
                "SA_DTS_CellTempMinTop5ID": [
                    "1",
                    "1"
                ],
                "SI_DTS_CellTempMaxDiff": "2",
                "SI_DTS_PackPoleTempMax": "28",
                "SI_DTS_MaxPoleTempPosn": "1",
                "SI_DTS_PackPoleTempMin": "26",
                "SI_DTS_MinPoleTempPosn": "10",
                "SI_BSW_MMCUTemp": "0",
                "SI_BSW_SMCUTemp": "0",
                "SI_BSW_WireAbnormal": "0",
                "SI_BSW_AfeAccumVolt": "0",
                "SI_BSW_InternDiagnose": "0",
                "SI_DTS_PulsDchaCurrLimA": "170",
                "SI_BSW_DcdcRatedPwr": "0",
                "SI_BSW_DcdcMaxPwr": "0",
                "SI_DTS_MinPackVolt": "0",
                "SI_DTS_MaxPackVolt": "0",
                "SI_DTS_IsoR": "10000",
                "SI_ISM_NegRes": "0",
                "SI_ISM_PosRes": "0",
                "SI_BSW_SysRly_MosCtrl": "1",
                "SI_BSW_SysCirState": "1",
                "SI_BSW_PRlyStickSts": "0",
                "SI_BSW_NRlyStickSts": "0",
                "SI_BSW_BreakStickSts": "0",
                "SI_BSW_CANCounter": "0",
                "SI_BSW_SysUnuseCapAh": "280",
                "SI_BSW_SysChgPowerUpper": "3000",
                "SI_BSW_SysDiscPowerUpper": "3000",
                "SI_BSW_AveCellVolt": "3500",
                "SI_BSW_AveCellTemp": "26",
                "SA_BSW_CellVolMaxID_Pack": "1",
                "SA_BSW_CellVolMaxID_Cell": "11",
                "SA_BSW_CellVolMinID_Pack": "6",
                "SA_BSW_CellVolMinID_Cell": "10",
                "SA_BSW_CellTempMaxID_Pack": "8",
                "SA_BSW_CellTempMaxID_Cell": "1",
                "SA_BSW_CellTemplMinID_Pack": "1",
                "SA_BSW_CellTempMinID_Cell": "4"
            },
            "ChildrenData": [
                {
                    "Sn": "",
                    "No": 1,
                    "Type": "Pack",
                    "Name": "别名",
                    "ParamData": {
                        "SA_BSW_PackPoleTemp": [
                            "25",
                            "24"
                        ]
                    },
                    "ChildrenData": [
                        {
                            "Sn": "",
                            "No": 1,
                            "Type": "Cell",
                            "Name": "别名",
                            "ParamData": {
                                "SA_BSW_CellVolt": [
                                    "3206",
                                    "3298"
                                ],
                                "SA_BSW_CellTemp": [
                                    "23",
                                    "20"
                                ]
                            }
                        }
                    ]
                }
            ]
        },
        {
            "Sn": "cooling1234567890123456",
            "No": 1,
            "ComStatus": 1,
            "Type": "Cooling",
            "Name": "liquid",
            "ParamData": {
                "LiquidCool_LCOperatingSts": "2",
                "LiquidCool_LCInputOfSwitch": "0",
                "LiquidCool_LCRelayOutput": "0",
                "LiquidCool_LCSupplyTemp": "20",
                "LiquidCool_LCReturnTemp": "18",
                "LiquidCool_LCSysEvapInleTemp": "20",
                "LiquidCool_LCEvapOutletTemp": "18",
                "LiquidCool_LCAmbiTemp": "11",
                "LiquidCool_LCCondeTemp": "5",
                "LiquidCool_LCCurrOpenOfEleExpan": "0",
                "LiquidCool_LCWaterSuppPressure": "0",
                "LiquidCool_LCBackwaterPressure": "0",
                "LiquidCool_LCFanContrOutputVol": "5",
                "LiquidCool_LCCurrentCompressSpd": "0",
                "LiquidCool_LCOnOffCommand": "0",
                "LiquidCool_LCWorkMode": "0",
                "LiquidCool_LCCoolMode": "3",
                "LiquidCool_LCConTempSetTemp": "25",
                "LiquidCool_LC485CommAddress": "1",
                "LiquidCool_LCBAUD": "9600",
                "LiquidCool_LCFaultStatus1": "0",
                "LiquidCool_LCFaultStatus2": "0",
                "LiquidCool_LCFaultStatus3": "0",
                "LiquidCool_LCFaultStatus4": "0",
                "LiquidCool_LCFaultStatus8": "0",
                "LiquidCool_LCComState": "0"
            }
        },
        {
            "Sn": "fire1234567890123456",
            "No": 1,
            "ComStatus": 1,
            "Type": "Fire",
            "Name": "fighting",
            "ParamData": {
                "fire_obj_ComState": [
                    "1",
                    "1"
                ],
                "fire_obj_TotalState": "0",
                "fire_obj_Addr": [
                    "1",
                    "2"
                ],
                "fire_obj_Baud": [
                    "9600",
                    "9600"
                ],
                "fire_obj_InnerTemp": [
                    "25",
                    "25"
                ],
                "fire_obj_TempAlarmState": [
                    "0",
                    "0"
                ],
                "fire_obj_SmokeTransducerVolt": [
                    "200",
                    "290"
                ],
                "fire_obj_SmokeAlarmState": [
                    "0",
                    "0"
                ],
                "fire_obj_uwRelayCmd": [
                    "0",
                    "0"
                ],
                "fire_obj_uwResetCmd": [
                    "0",
                    "0"
                ]
            }
        },
        {
            "Sn": "temphum1234567890123456",
            "No": 1,
            "ComStatus": 1,
            "Type": "Tempandhum",
            "Name": "temphu",
            "ParamData": {
                "Tempandhum_ThComState": "0",
                "Tempandhum_Addr": "247",
                "Tempandhum_TempData": "20",
                "TempandhumValue_HumidityData": "30"
            }
        },
        {
            "Sn": "meter1234567890123456",
            "No": 1,
            "ComStatus": 1,
            "Type": "MeterParallel",
            "Name": "meter",
            "ParamData": {
                "stMeterParallel_uwComState": "1",
                "stMeterParallel_uwGridVoltRms": "220",
                "stMeterParallel_uwGridCurrRms": "5",
                "stMeterParallel_dwGridActivePower": "500",
                "stMeterParallel_dwGridWattlessPower": "100",
                "stMeterParallel_dwGridApparentPower": "530",
                "stMeterParallel_wTotalPowerFactor": "98",
                "stMeterParallel_uwGridFreqNew": "50",
                "stMeterParallel_dwGridTotalActivePower": "500",
                "stMeterParallel_dwGridTotalWattlessPower": "100",
                "stMeterParallel_dwGridTotalApparentPower": "530",
                "stMeterEnergyGdw_wActEnergyBuyA": "50",
                "stMeterEnergyGdw_wReactEnergyBuyA": "5",
                "stMeterEnergyGdw_wActEnergyBuyT": [
                    "50"
                ],
                "stMeterEnergyGdw_wReactEnergyBuyT": [
                    "5"
                ]
            }
        },
        {
            "Sn": "ups1234567890123456",
            "No": 1,
            "ComStatus": 1,
            "Type": "UpsData",
            "Name": "UpsDatavalue",
            "ParamData": {
                "UpsDataValue_UpsComState": "1",
                "UpsDataValue_UpsState": "1",
                "UpsDataValue_UpsId": "1",
                "UpsDataValue_GridErr": "0",
                "UpsDataValue_BatVoltLow": "0",
                "UpsDataValue_InnerAlarm": "0"
            }
        },
        {
            "Sn": "runstatus1234567890123456",
            "No": 1,
            "ComStatus": 1,
            "Type": "runstatus",
            "Name": "runinfo",
            "ParamData": {
                "AccessControlSts": "0",
                "FireSts": "0",
                "WaterImmerState": "0",
                "CircuitBreakerOperation": "0",
                "CircuitBreakerFlt": "0",
                "EmergencyStopSts": "0",
                "SurgeSts": "0",
                "FireAlarm": "0",
                "RunStatet": "1",
                "PowerCtr": "0",
                "AlarmState": "0"
            }
        }
    ]
}

const bmsaTest = {
    "MessageType": 604,
    "DeviceType": 0,
    "CcmSn": "23000SET23315011",
    "Time": "2024-1-10 13:20:00",
    "Time_uint64": 1673011675,
    "TotalCluster": 8,
    "Data": [
        {
            "Sn": "Cluster111",
            "No": 1,
            "ComStatus": 1,
            "Type": "Cluster",
            "Name": "簇别名",
            "ParamData": {
                "sys_rtc_year_month": "0",
                "sys_rtc_day_hour": "0",
                "sys_rtc_minute_second": "0",
                "SI_DTS_SysModeSts": "3",
                "SI_BSW_AfeComSts": "0",
                "SI_BSW_SlaveMcuComSts": "0",
                "SI_BSW_PackLinkVolt": "1391.5",
                "SI_BSW_AfterBreakLinkVolt": "0",
                "SI_BSW_OutputLinkVolt": "0",
                "SI_BSW_PackCurrDir": "2",
                "SI_BSW_PackCurrA": "110.8",
                "SI_BSW_PackOutputPower": "0",
                "SI_DTS_DisplaySOC": "99.4",
                "SI_DTS_DisplaySOH": "100",
                "SI_BSW_BatCtrlStatusMsg": "0",
                "SI_DTS_PRly_DiscMosSts": "1",
                "SI_DTS_NRly_ChgMosSts": "1",
                "SI_DTS_LmtChgMduSts": "0",
                "SI_DTS_PreChgRlySts": "1",
                "SI_DTS_BatChargeVoltUpper": "1518.4",
                "SI_DTS_BatDischgVoltLower": "1206.4",
                "SI_DTS_BatAllowChargeCurrMax": "170",
                "SI_DTS_BatAllowDischgCurrMax": "170",
                "SI_DTS_BatForceChargeEnable": "0",
                "EI_BSW_SafetyStandardCode": "0",
                "SI_BSW_BCUEnviTemp": "23.8",
                "SI_BSW_MPosRly_DiscMosTemp": "28.0",
                "SI_BSW_MNegRly_ChgMosTemp": "27.8",
                "SI_BSW_CurrSampResTemp": "29.0",
                "SA_DTS_CellVoltMaxTop5": [
                    "3540",
                    "3536"
                ],
                "SA_DTS_CellVoltMaxTop5ID": [
                    "144",
                    "141"
                ],
                "SA_DTS_CellVoltMinTop5": [
                    "3486",
                    "3484"
                ],
                "SA_DTS_CellVoltMinTop5ID": [
                    "250",
                    "248"
                ],
                "SI_DTS_CellVoltMaxDiff": "45",
                "SA_DTS_CellTempMaxTop5": [
                    "28",
                    "26"
                ],
                "SA_DTS_CellTempMaxTop5ID": [
                    "187",
                    "186"
                ],
                "SA_DTS_CellTempMinTop5": [
                    "26",
                    "26"
                ],
                "SA_DTS_CellTempMinTop5ID": [
                    "1",
                    "1"
                ],
                "SI_DTS_CellTempMaxDiff": "2",
                "SI_DTS_PackPoleTempMax": "28",
                "SI_DTS_MaxPoleTempPosn": "1",
                "SI_DTS_PackPoleTempMin": "26",
                "SI_DTS_MinPoleTempPosn": "10",
                "SI_BSW_MMCUTemp": "0",
                "SI_BSW_SMCUTemp": "0",
                "SI_BSW_WireAbnormal": "0",
                "SI_BSW_AfeAccumVolt": "0",
                "SI_BSW_InternDiagnose": "0",
                "SI_DTS_PulsDchaCurrLimA": "170",
                "SI_BSW_DcdcRatedPwr": "0",
                "SI_BSW_DcdcMaxPwr": "0",
                "SI_DTS_MinPackVolt": "0",
                "SI_DTS_MaxPackVolt": "0",
                "SI_DTS_IsoR": "10000",
                "SI_ISM_NegRes": "0",
                "SI_ISM_PosRes": "0",
                "SI_BSW_SysRly_MosCtrl": "1",
                "SI_BSW_SysCirState": "1",
                "SI_BSW_PRlyStickSts": "0",
                "SI_BSW_NRlyStickSts": "0",
                "SI_BSW_BreakStickSts": "0",
                "SI_BSW_CANCounter": "0",
                "SI_BSW_SysUnuseCapAh": "280",
                "SI_BSW_SysChgPowerUpper": "3000",
                "SI_BSW_SysDiscPowerUpper": "3000",
                "SI_BSW_AveCellVolt": "3500",
                "SI_BSW_AveCellTemp": "26",
                "SA_BSW_CellVolMaxID_Pack": "1",
                "SA_BSW_CellVolMaxID_Cell": "11",
                "SA_BSW_CellVolMinID_Pack": "6",
                "SA_BSW_CellVolMinID_Cell": "10",
                "SA_BSW_CellTempMaxID_Pack": "8",
                "SA_BSW_CellTempMaxID_Cell": "1",
                "SA_BSW_CellTemplMinID_Pack": "1",
                "SA_BSW_CellTempMinID_Cell": "4"
            },
            "ChildrenData": [
                {
                    "Sn": "",
                    "No": 1,
                    "Type": "Pack",
                    "Name": "别名",
                    "ParamData": {
                        "SA_BSW_PackPoleTemp": [
                            "25",
                            "24"
                        ]
                    },
                    "ChildrenData": [
                        {
                            "Sn": "",
                            "No": 1,
                            "Type": "Cell",
                            "Name": "别名",
                            "ParamData": {
                                "SA_BSW_CellVolt": [
                                    "3206",
                                    "3298"
                                ],
                                "SA_BSW_CellTemp": [
                                    "23",
                                    "20"
                                ]
                            }
                        }
                    ]
                }
            ]
        },
        {
            "Sn": "cooling002",
            "No": 1,
            "ComStatus": 1,
            "Type": "Cooling",
            "Name": "liquid",
            "ParamData": {
                "LiquidCool_LCOperatingSts": "2",
                "LiquidCool_LCInputOfSwitch": "0",
                "LiquidCool_LCRelayOutput": "0",
                "LiquidCool_LCSupplyTemp": "20",
                "LiquidCool_LCReturnTemp": "18",
                "LiquidCool_LCSysEvapInleTemp": "20",
                "LiquidCool_LCEvapOutletTemp": "18",
                "LiquidCool_LCAmbiTemp": "11",
                "LiquidCool_LCCondeTemp": "5",
                "LiquidCool_LCCurrOpenOfEleExpan": "0",
                "LiquidCool_LCWaterSuppPressure": "0",
                "LiquidCool_LCBackwaterPressure": "0",
                "LiquidCool_LCFanContrOutputVol": "5",
                "LiquidCool_LCCurrentCompressSpd": "0",
                "LiquidCool_LCOnOffCommand": "0",
                "LiquidCool_LCWorkMode": "0",
                "LiquidCool_LCCoolMode": "3",
                "LiquidCool_LCConTempSetTemp": "25",
                "LiquidCool_LC485CommAddress": "1",
                "LiquidCool_LCBAUD": "9600",
                "LiquidCool_LCFaultStatus1": "0",
                "LiquidCool_LCFaultStatus2": "0",
                "LiquidCool_LCFaultStatus3": "0",
                "LiquidCool_LCFaultStatus4": "0",
                "LiquidCool_LCFaultStatus8": "0",
                "LiquidCool_LCComState": "0"
            }
        },
        {
            "Sn": "fire001",
            "No": 1,
            "ComStatus": 1,
            "Type": "Fire",
            "Name": "fighting",
            "ParamData": {
                "fire_obj_ComState": [
                    "1",
                    "1"
                ],
                "fire_obj_TotalState": "0",
                "fire_obj_Addr": [
                    "1",
                    "2"
                ],
                "fire_obj_Baud": [
                    "9600",
                    "9600"
                ],
                "fire_obj_InnerTemp": [
                    "25",
                    "25"
                ],
                "fire_obj_TempAlarmState": [
                    "0",
                    "0"
                ],
                "fire_obj_SmokeTransducerVolt": [
                    "200",
                    "290"
                ],
                "fire_obj_SmokeAlarmState": [
                    "0",
                    "0"
                ],
                "fire_obj_uwRelayCmd": [
                    "0",
                    "0"
                ],
                "fire_obj_uwResetCmd": [
                    "0",
                    "0"
                ]
            }
        },
        {
            "Sn": "temphu001",
            "No": 1,
            "ComStatus": 1,
            "Type": "Tempandhum",
            "Name": "temphu",
            "ParamData": {
                "Tempandhum_ThComState": "0",
                "Tempandhum_Addr": "247",
                "Tempandhum_TempData": "20",
                "TempandhumValue_HumidityData": "30"
            }
        },
        {
            "Sn": "meter001",
            "No": 1,
            "ComStatus": 1,
            "Type": "MeterParallel",
            "Name": "meter",
            "ParamData": {
                "stMeterParallel_uwComState": "1",
                "stMeterParallel_uwGridVoltRms": "220",
                "stMeterParallel_uwGridCurrRms": "5",
                "stMeterParallel_dwGridActivePower": "500",
                "stMeterParallel_dwGridWattlessPower": "100",
                "stMeterParallel_dwGridApparentPower": "530",
                "stMeterParallel_wTotalPowerFactor": "98",
                "stMeterParallel_uwGridFreqNew": "50",
                "stMeterParallel_dwGridTotalActivePower": "500",
                "stMeterParallel_dwGridTotalWattlessPower": "100",
                "stMeterParallel_dwGridTotalApparentPower": "530",
                "stMeterEnergyGdw_wActEnergyBuyA": "50",
                "stMeterEnergyGdw_wReactEnergyBuyA": "5",
                "stMeterEnergyGdw_wActEnergyBuyT": [
                    "50"
                ],
                "stMeterEnergyGdw_wReactEnergyBuyT": [
                    "5"
                ]
            }
        },
        {
            "Sn": "ups001",
            "No": 1,
            "ComStatus": 1,
            "Type": "UpsData",
            "Name": "UpsDatavalue",
            "ParamData": {
                "UpsDataValue_UpsComState": "1",
                "UpsDataValue_UpsState": "1",
                "UpsDataValue_UpsId": "1",
                "UpsDataValue_GridErr": "0",
                "UpsDataValue_BatVoltLow": "0",
                "UpsDataValue_InnerAlarm": "0"
            }
        },
        {
            "Sn": "runstatus1234567890123456",
            "No": 1,
            "ComStatus": 1,
            "Type": "runstatus",
            "Name": "runinfo",
            "ParamData": {
                "AccessControlSts": "0",
                "FireSts": "0",
                "WaterImmerState": "0",
                "CircuitBreakerOperation": "0",
                "CircuitBreakerFlt": "0",
                "EmergencyStopSts": "0",
                "SurgeSts": "0",
                "FireAlarm": "0",
                "RunStatet": "1",
                "PowerCtr": "0",
                "AlarmState": "0"
            }
        }
    ]
}

const bms = {
    "MessageType": 604,
    "DeviceType": 0,
    "CcmSn": "21000SEA12800778",
    "Time": "2024-1-8 16:27:55",
    "Time_uint64": 1673011675,
    "CreationDate": "2024-01-22 08:00:00",
    "ServerArea": "CN",
    "Data": {
        "DataLoggerSN": "5050KXXX181S0100",
        "TotalCluster": 8,
        "ESSData": [
            {
                "Sn": "1234567890123456_Cluster_1",
                "No": 1,
                "ComStatus": 0,
                "Type": "Cluster",
                "Name": "簇别名",
                "ParamData": {
                    "year_month": 0,
                    "day_hour": 0,
                    "minute_second": 0,
                    "SI_DTS_SysModeSts": 3,
                    "SI_BSW_AfeComSts": 0,
                    "SI_BSW_SlaveMcuComSts": 0,
                    "SI_BSW_PackLinkVolt": 1391.5,
                    "SI_BSW_AfterBreakLinkVolt": 0,
                    "SI_BSW_OutputLinkVolt": 0,
                    "SI_BSW_PackCurrDir": 2,
                    "SI_BSW_PackCurrA": 110.8,
                    "SI_BSW_PackOutputPower": 0,
                    "SI_DTS_DisplaySOC": 99.4,
                    "SI_DTS_DisplaySOH": 100,
                    "SI_BSW_BatCtrlStatusMsg": 0,
                    "SI_DTS_PRly_DiscMosSts": 1,
                    "SI_DTS_NRly_ChgMosSts": 1,
                    "SI_DTS_LmtChgMduSts": 0,
                    "SI_DTS_PreChgRlySts": 1,
                    "SI_DTS_BatChargeVoltUpper": 1518.4,
                    "SI_DTS_BatDischgVoltLower": 1206.4,
                    "SI_DTS_BatAllowChargeCurrMax": 170,
                    "SI_DTS_BatAllowDischgCurrMax": 170,
                    "SI_DTS_BatForceChargeEnable": 0,
                    "EI_BSW_SafetyStandardCode": 0,
                    "SI_BSW_BCUEnviTemp": 23.8,
                    "SI_BSW_MPosRly_DiscMosTemp": 28,
                    "SI_BSW_MNegRly_ChgMosTemp": 27.8,
                    "SI_BSW_CurrSampResTemp": 29,
                    "SA_DTS_CellVoltMaxTop5": [
                        3540,
                        3536
                    ],
                    "SA_DTS_CellVoltMaxTop5ID": [
                        144,
                        143
                    ],
                    "SA_DTS_CellVoltMinTop5": [
                        3486,
                        3487
                    ],
                    "SA_DTS_CellVoltMinTop5ID": [
                        250,
                        251
                    ],
                    "SI_DTS_CellVoltMaxDiff": 45,
                    "SA_DTS_CellTempMaxTop5": [
                        28,
                        28
                    ],
                    "SA_DTS_CellTempMaxTop5ID": [
                        187,
                        187
                    ],
                    "SA_DTS_CellTempMinTop5": [
                        26,
                        26
                    ],
                    "SA_DTS_CellTempMinTop5ID": [
                        1,
                        1
                    ],
                    "SI_DTS_CellTempMaxDiff": 2,
                    "SI_DTS_PackPoleTempMax": 28,
                    "SI_DTS_MaxPoleTempPosn": 1,
                    "SI_DTS_PackPoleTempMin": 26,
                    "SI_DTS_MinPoleTempPosn": 10,
                    "SI_BSW_MMCUTemp": 0,
                    "SI_BSW_SMCUTemp": 0,
                    "SI_BSW_WireAbnormal": 0,
                    "SI_BSW_AfeAccumVolt": 0,
                    "SI_BSW_InternDiagnose": 0,
                    "SI_DTS_PulsDchaCurrLimA": 170,
                    "SI_BSW_DcdcRatedPwr": 0,
                    "SI_BSW_DcdcMaxPwr": 0,
                    "SI_DTS_MinPackVolt": 0,
                    "SI_DTS_MaxPackVolt": 0,
                    "SI_DTS_IsoR": 10000,
                    "SI_ISM_NegRes": 0,
                    "SI_ISM_PosRes": 0,
                    "SI_BSW_SysRly_MosCtrl": 1,
                    "SI_BSW_SysCirState": 1,
                    "SI_BSW_PRlyStickSts": 0,
                    "SI_BSW_NRlyStickSts": 0,
                    "SI_BSW_BreakStickSts": 0,
                    "SI_BSW_CANCounter": 0,
                    "SI_BSW_SysUnuseCapAh": 280,
                    "SI_BSW_SysChgPowerUpper": 3000,
                    "SI_BSW_SysDiscPowerUpper": 3000,
                    "SI_BSW_AveCellVolt": 3500,
                    "SI_BSW_AveCellTemp": 26,
                    "SA_BSW_CellVolMaxID_Pack": 1,
                    "SA_BSW_CellVolMaxID_Cell": 11,
                    "SA_BSW_CellVolMinID_Pack": 6,
                    "SA_BSW_CellVolMinID_Cell": 10,
                    "SA_BSW_CellTempMaxID_Pack": 8,
                    "SA_BSW_CellTempMaxID_Cell": 1,
                    "SA_BSW_CellTemplMinID_Pack": 1,
                    "SA_BSW_CellTempMinID_Cell": 4,
                    "AccumChgCapAh": 174.6,
                    "AccumDischgCapAh": 1.1,
                    "AccumChgEgyKwh": 246.2,
                    "AccumDischgEgyKwh": 0,
                    "MainCodeHexData": "00004300",
                    "SunCodeHexData": "00004300"
                },
                "ChildrenData": [
                    {
                        "Sn": "1234567890123456_Cluster_1_Pack_1",
                        "No": 1,
                        "Type": "Pack",
                        "Name": "别名",
                        "ParamData": {
                            "SA_BSW_PackPoleTemp": [
                                25,
                                24
                            ]
                        },
                        "ChildrenData": [
                            {
                                "Sn": "1234567890123456_Cluster_1_Pack_1_Cell_1",
                                "No": 1,
                                "Type": "Cell",
                                "Name": "别名",
                                "ParamData": {
                                    "SA_BSW_CellVolt": [
                                        3206,
                                        3298
                                    ],
                                    "SA_BSW_CellTemp": [
                                        23,
                                        24
                                    ]
                                }
                            }
                        ]
                    }
                ]
            },
            {
                "Sn": "cooling1234567890123456",
                "No": 1,
                "ComStatus": 1,
                "Type": "Cooling",
                "Name": "liquid",
                "ParamData": {
                    "LCOperatingSts": 2,
                    "LCInputOfSwitch": 0,
                    "LCRelayOutput": 0,
                    "LCSupplyTemp": 20,
                    "LCReturnTemp": 18,
                    "LCSysEvapInleTemp": 20,
                    "LCEvapOutletTemp": 18,
                    "LCAmbiTemp": 11,
                    "LCCondeTemp": 5,
                    "LCCurrOpenOfEleExpan": 0,
                    "LCWaterSuppPressure": 0,
                    "LCBackwaterPressure": 0,
                    "LCFanContrOutputVol": 5,
                    "LCCurrentCompressSpd": 0,
                    "LCOnOffCommand": 0,
                    "LCWorkMode": 0,
                    "LCCoolMode": 3,
                    "LCConTempSetTemp": 25,
                    "LC485CommAddress": 1,
                    "LCBAUD": 9600,
                    "LCFaultStatus1": 0,
                    "LCFaultStatus2": 0,
                    "LCFaultStatus3": 0,
                    "LCFaultStatus4": 0,
                    "LCFaultStatus8": 0,
                    "LCComState": 0
                }
            },
            {
                "Sn": "1234567890123456_Fire_1",
                "No": 1,
                "ComStatus": 1,
                "Type": "Fire",
                "Name": "fighting",
                "ParamData": {
                    "Fire_ComState": [
                        1,
                        1
                    ],
                    "Fire_TotalState": 0,
                    "Fire_Addr": [
                        1,
                        2
                    ],
                    "Fire_Baud": [
                        9600,
                        9600
                    ],
                    "Fire_InnerTemp": [
                        25,
                        25
                    ],
                    "Fire_TempAlarmState": [
                        0,
                        0
                    ],
                    "Fire_SmokeTransducerVolt": [
                        200,
                        290
                    ],
                    "Fire_SmokeAlarmState": [
                        0,
                        0
                    ],
                    "Fire_RelayCmd": [
                        0,
                        0
                    ],
                    "Fire_ResetCmd": [
                        0,
                        0
                    ]
                }
            },
            {
                "Sn": "1234567890123456_Tempandhum_1",
                "No": 1,
                "ComStatus": 1,
                "Type": "Tempandhum",
                "Name": "temphu",
                "ParamData": {
                    "Tempandhum_ThComState": 0,
                    "Tempandhum_Addr": 247,
                    "Tempandhum_TempData": 20,
                    "Tempandhum_HumidityData": 30
                }
            },
            {
                "Sn": "1234567890123456_MeterParallel_1",
                "No": 1,
                "ComStatus": 1,
                "Type": "MeterParallel",
                "Name": "meter",
                "ParamData": {
                    "Meter_ComState": 1,
                    "Meter_VoltRms": 220,
                    "Meter_CurrRms": 5,
                    "Meter_ActivePower": 500,
                    "Meter_WattlessPower": 100,
                    "Meter_ApparentPower": 530,
                    "Meter_TotalPowerFactor": 98,
                    "Meter_FreqNew": 50,
                    "Meter_TotalActivePower": 500,
                    "Meter_TotalWattlessPower": 100,
                    "Meter_TotalApparentPower": 530,
                    "Meter_ActEnergyBuyA": 50,
                    "Meter_ReactEnergyBuyA": 5,
                    "Meter_ActEnergyBuyT": [
                        50,
                        50,
                        50
                    ],
                    "Meter_ReactEnergyBuyT": [
                        5,
                        5,
                        5
                    ]
                }
            },
            {
                "Sn": "1234567890123456_UpsData_1",
                "No": 1,
                "ComStatus": 1,
                "Type": "UpsData",
                "Name": "UpsDatavalue",
                "ParamData": {
                    "Ups_UpsComState": 1,
                    "Ups_UpsState": 1,
                    "Ups_UpsId": 1,
                    "Ups_GridErr": 0,
                    "Ups_BatVoltLow": 0,
                    "Ups_InnerAlarm": 0
                }
            },
            {
                "Sn": "1234567890123456_RunStatus_1",
                "No": 1,
                "ComStatus": 1,
                "Type": "runstatus",
                "Name": "runinfo",
                "ParamData": {
                    "AccessControlSts": 0,
                    "FireSts": 0,
                    "WaterImmerState": 0,
                    "CircuitBreakerOperation": 0,
                    "CircuitBreakerFlt": 0,
                    "EmergencyStopSts": 0,
                    "SurgeSts": 0,
                    "FireAlarm": 0,
                    "RunStatet": 1,
                    "PowerCtr": 0,
                    "AlarmState": 0
                }
            }
        ]
    }
}


const pcs = {
    "MessageType": 11,
    "DeviceType": 0,
    "CcmSn": "21000SEA12800778",
    "Time": "2023-12-13 13:40:23",
    "Time_uint64": 1702474823,
    "CreationDate": "2024-01-19 08:00:00",
    "RunningMode": 2,
    "Data": [
        {
            "InverterSn": "inverter_2300KBTH15CL0100",
            "Type": 1,
            "RunningStatus": 2,
            "PVPower": 0,
            "VacL1": 0,
            "VacL2": 0,
            "VacL3": 0,
            "Vac1": 0,
            "Vac2": 0,
            "Vac3": 0,
            "Iac1": 0,
            "Iac2": 0,
            "Iac3": 0,
            "DayPeakPac": 0,
            "Pac": 0,
            "ReactivePower": 0,
            "PF": 0,
            "Fac": 0,
            "Efficiency": 0,
            "InnerTemperature": 177.6,
            "IsoImpedance": 0,
            "TurnOnTime": 0,
            "TurnOffTime": 0,
            "ETotal": 0,
            "EDay": 0,
            "Emonth": 0,
            "Eyear": 0,
            "V_FactorA": 0,
            "V_FactorB": 0,
            "V_FactorC": 0,
            "P_AcPosi": 0,
            "P_AcNega": 0,
            "P_RacPosi": 0,
            "P_RacNega": 0,
            "DCI_A": 0,
            "DCI_B": 0,
            "DCI_C": 0,
            "GFCI_C": 0,
            "GFCI_R": 0,
            "GridHours": 0,
            "WorkHours": 0,
            "FailureHours": 0,
            "ConnectedHours": 0,
            "RelayTimes": 0,
            "IphaseA": 0,
            "IphaseB": 0,
            "IphaseC": 0,
            "THDiPhaseA": 0,
            "THDuPVPhaseA": 0,
            "THDuLVPhaseA": 0,
            "THDiPhaseB": 0,
            "THDuPVPhaseB": 0,
            "THDuLVPhaseB": 0,
            "THDiPhaseC": 0,
            "THDuPVPhaseC": 0,
            "THDuLVPhaseC": 0,
            "ModulationMode": 0,
            "CphaseA": 0,
            "CphaseB": 0,
            "CphaseC": 0,
            "Vnpe": 0,
            "DC_Link": 0,
            "C_FilterA": 0,
            "C_FilterB": 0,
            "C_FilterC": 0,
            "R_Grid": 0,
            "X_Grid": 0,
            "PVPositiveGroundVoltage": 2.4,
            "PVNegativeGroundVoltage": 0,
            "V_Positive": 0,
            "V_Negative": 0,
            "Standby1": 0,
            "DSPSVNSoftVersion1": 2,
            "CPLDCode": 255,
            "HTotal": 0,
            "GFCICheckValue_SafetyConutry": 32,
            "BUSVoltage": 2.4,
            "NBUSVoltage": 0,
            "RModuleTemp": 185.8,
            "SModuleTemp": 159.7,
            "TModuleTemp": 185.8,
            "Tamb": 177.6,
            "LeakCurrent_Avg": 0,
            "HvrtTimes": 0,
            "LvrtTimes": 0,
            "OverCurrentTimes": 0,
            "LeakCurrentMean": 0,
            "C_Temper": 169.4,
            "SpeedFan1": 0,
            "SpeedFan2": 0,
            "SpeedFan3": 0,
            "SpeedFan4": 0,
            "SpeedFan5": 0,
            "SpeedFan6": 0,
            "SpeedFan7": 0,
            "SpeedFan8": 0,
            "SpeedFan9": 0,
            "SpeedFan10": 0,
            "InternalFanDuty": 0.01,
            "ExternalFanDuty": 0.01,
            "BusCapacitorLife": 0,
            "RModule2Temp": 0,
            "SModule2Temp": 0,
            "TModule2Temp": 0,
            "DeratingFlagNow": 0,
            "DeratingRefNow": 0,
            "ReactiveFlagNow": 0,
            "EffectiveReactivePower": 0,
            "P_Acdispatch": 110,
            "P_ArpLimit": 0,
            "P_RcLimit": 0,
            "P_ApReference": 0,
            "V_DC": 0,
            "C_DC": 0,
            "Reserved12": 0,
            "Reserved13": 0,
            "Reserved14": 0,
            "Reserved15": 0,
            "Reserved16": 0,
            "OffGridDiagnosis": 0,
            "MasterSlaveFlag": 0,
            "GridFault1": 8261,
            "GridFault2": 0,
            "SystemFault1": 0,
            "SystemFault2": 0,
            "DecviceFault1": 45056,
            "DecviceFault2": 0,
            "DCFault1": 0,
            "DCFault2": 0,
            "DCFault3": 0,
            "BatFault1": 0,
            "BatFault2": 0,
            "BatFault3": 0,
            "InnerFault1": 0,
            "InnerFault2": 0,
            "InnerFault3": 0,
            "InnerFault4": 0,
            "Warning1": 3,
            "MainCodeHexData": "00004300",
            "SunCodeHexData": "00004300",
            "String1Warning": 0,
            "String2Warning": 0,
            "FlashFaultWarning": 0,
            "Standby2": 0,
            "DeviceStatus": 0,
            "OperationFlag1": 0,
            "OperationFlag2": 0,
            "OperationFlag3": 0,
            "OperationFlag4": 0,
            "OperationFlag5": 0,
            "OperationFlag6": 0,
            "OperationFlag7": 0,
            "OperationFlag8": 0,
            "FunctionStatus1": 98,
            "FunctionStatus2": 24576,
            "FunctionStatus3": 59393,
            "FunctionStatus4": 38304,
            "FunctionStatus5": 0,
            "FunctionStatus6": 0
        }
    ],
    "isParallelMachine": "false"
}


const meter = {
    "MessageType": 612,
    "DeviceType": 0,
    "CcmSn": "21000SEA12800778",
    "Time": "2023-11-17 09:59:52",
    "Time_uint64": 1700215192,
    "CreationDate": "2024-01-19 08:00:00",
    "Data": [
        {
            "MeterSN": "METER_23000EZR23521116",
            "MeterType": "Grid_Meter",
            "MeterCOMMStatus": 1,
            "VoltageAVal": 225.7,
            "VoltageBVal": 0,
            "VoltageCVal": 0,
            "CurrentAVal": 0.89,
            "CurrentBVal": 0,
            "CurrentCVal": 0,
            "ActiveAVal": 0.094,
            "ActiveBVal": 0,
            "ActiveCVal": 0,
            "ActiveVal": 0.094,
            "ReactiveAVal": -0.033,
            "ReactiveBVal": 0,
            "ReactiveCVal": 0,
            "ReactiveVal": -0.033,
            "DependingPowerAVal": 0.201,
            "DependingPowerBVal": 0,
            "DependingPowerCVal": 0,
            "DependingPowerVal": 0.103,
            "PowerFactorAVal": 0.495,
            "PowerFactorBVal": 0,
            "PowerFactorCVal": 0,
            "PowerFactorVal": 0.951,
            "Frequency": 50.03,
            "ForwardAPowerSell": 0,
            "ForwardBPowerSell": 0,
            "ForwardCPowerSell": 0,
            "ForwardPowerSell": 9.43,
            "ForwardAPowerBuy": 0,
            "ForwardBPowerBuy": 0,
            "ForwardCPowerBuy": 0,
            "ForwardPowerBuy": 0,
            "ReverseAPowerSell": 0,
            "ReverseBPowerSell": 0,
            "ReverseCPowerSell": 0,
            "ReversePowerSell": 0,
            "ReverseAPowerBuy": 0,
            "ReverseBPowerBuy": 0,
            "ReverseCPowerBuy": 0,
            "ReversePowerBuy": 9.43
        }
    ]
}


const ems = {
    "MessageType": 3,
    "DeviceType": 0,
    "CcmSN": "21000SEA12800778",
    "RSSI": 80,
    "COMMode": "WIFI",
    "Errormessage": "Uart comm fail",
    "CcmVer": "V4.0.3",
    "Timestamp": "2021-3-12 12:34:12",
    "CreationDate": "2024-01-19 08:00:00",
    "MainCodeHexData": "000000000",
    "SunCodeHexData": "000000000"
}

const randomDefine = {
    "messageType|+1": [604, 11, 612],
    "deviceType": 0,
    "sn": /^20001GTW1180700[1234]{1}$/,
    "time": '',
    "ts": 0
}

const randomWaterSensor = {
    'ts': 0,
    'id': /^ws_00[12345678]{1}$/,
    'vc|+1': 1
}

const getRandomMessage = function() {
    let data = Mock.mock(randomWaterSensor);
    // data.time = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
    data.ts = new Date().getTime();

    console.log(data);
    return data;
}


module.exports = {
    bmsa,
    bmsaTest,
    bms,
    pcs,
    meter,
    ems,
    getRandomMessage
}