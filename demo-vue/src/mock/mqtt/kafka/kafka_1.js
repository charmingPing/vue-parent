// 引入组件
const kafka = require('kafka-node');
const client = new kafka.KafkaClient({
    kafkaHost: '127.0.0.1:9092'
});


// 创建topic
createTopics();

// 启动消息消费
// createConsumer((message) => {
//     console.log('event callback receive message:');
//     console.log(message);
// });

// 创建生产者
createHighLevelProducter((producter) => {
    // #对主题：topic-for-1的第1个分区发送消息，对主题：topic-for-8的第2个分区发送消息。
    setTimeout(() => {
        const record = [
            {
                topic: 'topic-for-1',
                message: "Hello topic1!",
                key: 'topic1-1',
                attributes: 1,
                partition: 0
            },
            {
                topic: 'topic-for-8',
                message: "Hello topic8。",
                key: 'topic8-1',
                attributes: 1,
                partition: 1
            }
        ];
        producter.send(record, (err, data) => {
            if (err) {
                console.log(`producer send err: ${err}`)
            }
        });
    }, 500);
});

// #创建两个topic
// #其中主题为topic-for-1 拥有1个分区数
// #主为 topic-for-8 拥有4个分区数
function createTopics() {
    const topicsToCreate = [
        {
            topic: 'topic-for-1',
            partitions: 1,     //分区数量
            replicationFactor: 1        //副本数量
        },
        {
            topic: 'topic-for-8',
            partitions: 4,           //分区数量
            replicationFactor: 1,     //副本数量
            // Optional set of config entries
            configEntries: [
                {
                    name: 'compression.type',
                    value: 'gzip'
                },
                {
                    name: 'min.compaction.lag.ms',
                    value: '50'
                }
            ]
        }];

    client.createTopics(topicsToCreate, (error, result) => {
        if (error) {
            console.log(`create topics err: ${err}`)
        }
    });
}

/**
 * 创建消费者
 * @param {*} callback 
 * @returns 
 */
function createConsumer(callback) {
    //消费者
    let self = this;
    let consumer = new kafka.Consumer(this.client,
        [
            { topic: 'topic-for-1', partition: 0, groupId: "my_group" },
            { topic: 'topic-for-8', partition: 1 }

            // #注：监听两个topic，分别名为topic-for-1，topic-for-8。其中
            // #监听topic-for-1的0号分区，设置组名为 my_group。
            // #即如果还有机器加入，并且组名也为my_group时，则代表同属同一个组。
        ],
        {
            autoCommit: true    //为true，代表自动提交offset
        }
    );
    consumer.on('message', function (message) {
        console.log('consumer receive message:')
        console.log(message);
        callback(message);
    });

    consumer.on('error', function (err) {
        console.log('consumer err:')
        console.log(err);
    });
    return consumer;
}

/**
 * 高可用kafka生产者
 * @param {*} callback 
 */
function createHighLevelProducter(callback) {
    const producer = new kafka.HighLevelProducer(client);
    producer.on('ready', function () {
        console.log('kafka producer is connected and ready');
        callback(producer);
    });
    producer.on('error', function (err) {
        console.log('kafka producer is error');
    });
}