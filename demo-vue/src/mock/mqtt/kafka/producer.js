const kafka = require('kafka-node');
const moment = require('moment');

const client = new kafka.KafkaClient({ kafkaHost: '192.168.1.214:9092' });
const producer = new kafka.HighLevelProducer(client);
producer.on('ready', function () {
    console.log('Kafka Producer is connected and ready.')
})
producer.on('error', function (error) {
    console.error(error)
})

const sendRecord = (objData, callback) => {
    const buffer = Buffer.from(JSON.stringify(objData))
    const record = [
        {
            topic: 'water-sensor-dev',
            messages: buffer,
            attributes: 1 /* Use GZip compression for the payload */
        }
    ]
    producer.send(record, callback)
}


/**
 * 
 * 消息发送
 */
const mockData = require('../data');


const cb = (err, data) => {
    if (err) {
        console.log(`err: ${err}`)
    }
    console.log(`data: ${JSON.stringify(data)}`)
}

// 定时发送
let i = 0;
setInterval(() => {

    let data = mockData.getRandomMessage();
    sendRecord(data, cb);

    i++;
}, 5 * 1000);