/**
 * https://juejin.cn/post/7066811231230754823
 * https://www.jianshu.com/p/9dbcfbe6130f
 * https://blog.csdn.net/weixin_45232840/article/details/105918908
 * 
 * 模拟数据 - 单独的模块
 */
const Mock = require('mockjs');
const query = require('./db') // 根据需要连接数据库

const demo = Mock.mock({
    "data|3": [
        {
            "value|+1": 1,
            "label|+1": ["demo1", "demo2", "demo3"]
        }
    ]
});

const user = Mock.mock({
    "id": 1,
    "name": '@cname',
    'age|10-20': 1
})

const listUser = Mock.mock({
    'data|53': [
        {
            'id|+1': 1,
            'username': '@last',
            'name': '@cname',
            'age|10-60': 1,
            'email': '@email',
            'avatar': '@image',
            // 'gender': '@boolean',
            'gender|1': [0, 1],
            'phone': /^1[34578]\d{9}$/,
            'address': '@province@city@county',
            'birthday': '@datetime("yyyy-MM-dd HH:mm:ss")',
            'signature': '@cparagraph(1)'
        }
    ]
});

const listMenu = [
    {
        key: '/',
        title: '首页',
        path: '/',
        icon: 'home'
    },
    {
        key: '/system',
        title: '系统管理',
        path: '/system',
        icon: 'solution',
        children: [
            {
                key: '/menu',
                title: '菜单管理',
                path: '/menu',
                icon: 'audit'
            },
            {
                key: '/user',
                title: '用户管理',
                path: '/user',
                icon: 'coffee'
            },
            {
                key: '/dictionary',
                title: '字典管理',
                path: '/dictionary',
                icon: 'global'
            }
        ]
    },
    {
        key: '/setting',
        title: '设置',
        path: '/setting',
        icon: 'setting',
        children: [
            {
                key: '/account',
                title: '个人中心',
                path: '/account',
                icon: 'user'
            },
            {
                key: '/language',
                title: '语言',
                path: '/language',
                icon: 'pound'
            },
            {
                key: '/theme',
                title: '主题',
                path: '/theme',
                icon: 'alert'
            }
        ]
    }
];

// 数据库查询结果
let listElectric = query('select * from electric_meter');

let getUserList = function(values) {
    return query('select * from t_user limit ?,?', values);
}

let getTotal = function(values) {
    return query('select count(1) as total from t_user', values);
}

// 
let getUserByUsername = function(values) {
    return query('select * from t_user where username = ?', values);
}

let getUserById = function(values) {
    return query('select * from t_user where id = ?', values);
}

// 数据库查询结果
let addUser = function(values) {
    return query(`insert into t_user(username, uid, name, gender, age, birthday, phone, email, address, signature, tag, avatar, status, delete_flag, create_by, create_time) 
    values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, values);
}

/**
 * https://www.jianshu.com/p/aaf912d7329e
 * https://segmentfault.com/q/1010000010846519
 * exports = { user } 这么写，在引用的地方是undefined
 */
module.exports = {
    demo,
    user,
    listUser,
    listMenu,
    listElectric,
    addUser,
    getUserByUsername,
    getUserById,
    getUserList,
    getTotal
}


// //mockjs 文档的github地址: https://github.com/nuysoft/Mock/wiki
// let basicData = Mock.mock({
//     'list|1-100': [{
//         'id|+1': 1,
//         'isBoolean': '@boolean(10, 0, true)',//百分之百的true
//         'naturalNumber': '@natural(1, 1000)', //大于等于零的整数
//         'integer': '@integer(0)', //随机整数
//         'float': '@float(1, 100, 3, 6)', //随机浮点数,
//         'character': '@character("upper")', //一个随机字符
//         'string': '@string("lower", 5, 20)', //一串随机字符串
//         'range': '@range(1, 10, 2)', //一个整形数组，步长为2
//     }]
// });

// // console.log(basicData);

// let Date = Mock.mock({
//     'dateList|10': [{
//         'date': '@date',
//         'date-yyyy-MM-dd': '@date(yyyy-MM-dd)',
//         'date-yy-MM-dd': '@date(yy-MM-dd)',
//         'date-y-MM-dd': '@date(y-MM-dd)',
//         'date-y-M-d': '@date(y-M-d)',
//         'line-through': '------------------------------------------------',
//         'timessss': '@time', //随机的时间字符串,
//         'time-format': '@time()', //指示生成的时间字符串的格式, default: 'HH: mm: ss',
//         'time-format-1': '@time("A HH:mm:ss")',
//         'time-format-2': '@time("a HH:mm:ss")',
//         'time-format-3': '@time("HH:mm:ss")',
//         'time-format-4': '@time("H:m:s")',
//         'datetime': '@datetime("yyyy-MM-dd A HH:mm:ss")', //返回一个随机的日期和时间字符串
//         'dateNow': '@now("second")' //获取当前完整时间
//     }]
// });
// // console.log(Date);

// let imageList = Mock.mock({
//     'imageList|5': [{
//         'id|+1': 1,
//         'img': '@image',//生成一个随机的图片地址,
//         'img-1': '@image("200x100", "#000", "#fff", "png", "Mock.js")', //生成一个200*100, 背景色#000，前景色#fff, 格式png, 文字mock.js的图片
//     }]
// })
// // console.log(imageList);

// let paragraph = Mock.mock({
//     'paragraphList|5': [{
//         'id|+1': 1,
//         'paragraph1': '@cparagraph(2)', //生成一段2句话的中文文本,
//         'paragraph2': '@paragraph(3)', //生成一个3句话的英文文本
//         'title': '@title', //随机生成一个英文标题
//         'ctitle': '@ctitle', //随机生成一个中文标题
//     }]
// })
// // console.log(paragraph);

// let name = Mock.mock({
//     'nameList|5': [{
//         'id|+1': 1,
//         'name': '@name', //英文名,
//         'cname': '@cname', //中文名
//     }]
// })
// // console.log(name);

// let webList = Mock.mock({
//     'webList|5': [{
//         'id|+1': 0,
//         'url': '@url("http", "baidu.com")', //url: http://www.baidu.com
//         'protocol': '@protocol', //随机生成一个url协议
//         'domain': '@domain', //随机生成一个域名,
//         'email': '@email', //随机生成一个邮箱地址,
//         'ip': '@ip' //随机生成一个ip地址
//     }]
// })

// let address = Mock.mock({
//     'addressList|5': [{
//         'id|+1': 1,
//         'region': '@region', //生成一个大区
//         'province': '@province', //生成一个省份
//         'city': '@city', //生成一个市
//         'country': '@country', //一个县
//         'zip': '@zip', //邮政编码
//     }]
// })

