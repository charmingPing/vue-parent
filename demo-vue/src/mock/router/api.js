// user.js - 路由模块
const express = require('express');
const router = express.Router();


// 引入验证码
const svgCaptcha = require('svg-captcha');

// 引入数据
const mockData = require('../data');
const uuid = require('node-uuid');


// 主页路由
router.get('/', (req, res) => {
  res.send('维基主页');
});

// “关于页面”路由
router.get('/about', (req, res) => {
  res.send('关于此维基');
});

router.get("/demo", function (req, res) {
  res.ok(mockData.demo.data);
});

// 登录
router.post("/login", function (req, res) {
  let param = req.getParams()
  console.log('session.captcha:', req.session.captcha)
  console.log('captcha:', param.captcha)
  if ('123456' === param.password) {
    let user = {
      id: 1,
      username: param.username,
      password: param.password,
      token: 'abcs-oasd-aksgjh-aksjglk-12asdgsg'
    }
    if ('admin' === param.username) {
      user.avatar = 'https://ts1.cn.mm.bing.net/th/id/R-C.152c6bef34ee3fc8129dd2c4e6d7a0af?rik=z4qH8z23QB4Qbg&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2017022316%2fszoovl343zm.jpg&ehk=nSw8w3CS7ai4nadwYICGNoJHvbkooCc1y1TKAy5rXYE%3d&risl=&pid=ImgRaw&r=0'
    }
    return res.ok(user)
  }
  return res.error('用户名或密码错误')
});

// 验证码
router.get("/captcha", function (req, res) {
  var codeConfig = {
    size: 4, // 验证码长度
    ignoreChars: '0o1i', // 验证码字符中排除 0o1i
    noise: 1, // 干扰线条的数量
    height: 40,
    fontSize: 38,
    color: true,
    background: '#eee'
  }
  var captcha = svgCaptcha.create(codeConfig);
  req.session.captcha = captcha.text.toLowerCase(); //存session用于验证接口获取文字码
  res.type('svg');
  res.send(captcha.data);
});

// 用户
router.post("/user", function (req, res) {
  res.ok(mockData.user);
});

router.get("/user/getUserById", async function (req, res) {
  let param = req.getParams();
  let result = await mockData.getUserById([param.id]);
  res.ok(result.length ? result[0] : {});
});

// 用户列表
router.get("/user/list", async function (req, res) {
  let param = req.getParams();
  let aaa = await mockData.getUserList([(param.pageNo - 1) * param.pageSize, parseInt(param.pageSize)]);
  let total = await mockData.getTotal();

  let result = {
    data: aaa,
    total: total[0].total
  }
  res.ok(result);
});

router.post("/user/edit", function (req, res) {
  let param = req.getParams();
  res.ok('保存成功');
});

// 添加用户
router.post("/user/add", async function (req, res, next) {
  let param = req.getParams();

  let values = [
    param.username,
    uuid.v1(),
    param.name,
    param.gender,
    param.age,
    param.birthdy,
    param.phone,
    param.email,
    param.address,
    param.signature,
    param.tag,
    param.avatar,
    param.status,
    param.delete_flag,
    param.create_by,
    param.create_time
  ];
  try {
    let user = await mockData.getUserByUsername([param.username]);
    if (!user.length) {
      let result = await mockData.addUser(values);
      res.ok('添加成功');
    } else {
      res.error('用户名已存在');
    }
	} catch (ex) {
		next(ex);
	}
});

// 菜单列表
router.get("/menu/list", function (req, res) {
  let param = req.getParams();
  res.ok(mockData.listMenu);
});

// 查询数据库例子
router.get("/electric/list", async function (req, res) {
  let param = req.getParams();
  // mockData.listElectric() 是错的，listElectric不是一个方法
  let result = await mockData.listElectric;
  res.ok(result);
});


module.exports = router;