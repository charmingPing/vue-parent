var express = require('express');
var expressWs = require('express-ws');

var router = express.Router();
expressWs(router);

router.ws('/:id', function (ws, req) {
    console.log('id: ', req.params.id);

    ws.on('message', function (msg) {
        ws.send('default response')
    })

    ws.on('close', function (e) {
        
    })
});

module.exports = router;