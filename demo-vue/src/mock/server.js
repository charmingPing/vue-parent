/**
 * https://www.cnblogs.com/soyxiaobi/p/9846057.html
 * https://blog.csdn.net/weixin_44813417/article/details/109532659
 * https://juejin.cn/post/6878208201486139400
 * [同时启动]https://www.cnblogs.com/fengzzi/p/12859151.html
 * [字体彩色]https://blog.csdn.net/wwppp987/article/details/109076664
 * [mock纯数组]https://blog.csdn.net/KimBing/article/details/122695741
 * [express request]https://juejin.cn/post/7174771540150452284
 * [express response]https://blog.csdn.net/qq_41761591/article/details/86467827
 * [express headers]https://kylebing.blog.csdn.net/article/details/128097491
 * [express filter]https://blog.csdn.net/qq_40635837/article/details/109552479
 * [express filter]https://blog.csdn.net/wb_001/article/details/79026038
 * [express log]https://juejin.cn/post/7085973336827625503
 * [express session 1]https://juejin.cn/post/7011327714254651428
 * [express session 2]https://www.cnblogs.com/mingjiatang/p/7495321.html
 * [express cookie]https://zhuanlan.zhihu.com/p/522417432
 * [跨域请求报错 1]https://blog.csdn.net/Julylyna/article/details/123229319
 * [跨域请求报错 2]https://blog.csdn.net/fesfsefgs/article/details/104950740
 * [跨域请求报错 3]https://blog.csdn.net/Loya0813/article/details/83862586
 * [java web captcha]https://blog.csdn.net/weixin_44869421/article/details/115051971
 * [好看的验证码]https://www.jianshu.com/p/3b237b9e4776
 * [模块拆分]https://www.jianshu.com/p/14dbe3274117
 * [模块拆分]https://segmentfault.com/q/1010000017854130
 * [模块拆分]https://juejin.cn/post/7202246519080206393
 * [路由信息]https://developer.mozilla.org/zh-CN/docs/Learn/Server-side/Express_Nodejs/routes
 * [websocket]https://juejin.cn/post/7024097945410600973
 * [DB]https://juejin.cn/post/6960311946390290469
 * 
 * Mock服务器，模拟后台接口
 * 运行: node src/mock/server.js
 */
const express = require('express');   // 引入express
const app = express();    // 实例化

const session = require('express-session')

// 使用express-session 来存放数据到session中
app.use(
    session({
        key: 'SESSION_ID',
        secret: "your_secret_key",
        resave: false,
        saveUninitialized: false,
        cookie: { maxAge: 1000 * 60 * 60 * 8, signed: true }
    })
);

/**
 * post方法
 */
var bodyParser = require("body-parser");
app.use(bodyParser.json()); // 添加json解析
app.use(bodyParser.urlencoded({ extended: false }));

/**
 * 为app添加中间件处理跨域请求
 */
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", req.headers.origin); 
    res.header("Access-Control-Allow-Credentials", true); // 允许带cookies
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Authorization"); // 放开Authorization，否则带了这个请求头的请求会被拦截
    next();
});

/**
 * token拦截, ws不走这里
 */
app.use(function (req, res, next) {
    if (req.method.toLowerCase() == 'options') {
        return res.sendStatus(200);  // 让options尝试请求快速结束
    }
    if (['/api/login', '/api/captcha'].includes(req.path)) {
        return next()
    }
    if (req.path.startsWith('/websocket')) {
        return next()
    }
    let token = req.get('Authorization') // 获取请求头，req.headers.Authorization 这个写法获取不到
    if (token) {
        return next()
    }
    // res.redirect('/login'); // 页面不跳转，network能看到重定向到了login
    res.status(401).json('尚未登录，登录后访问')
});

/**
 * 拦截所有请求，统一处理参数，结果返回
 */
app.all('/*', function (req, res, next) {
    // get, post 参数统一获取
    req.getParams = function () {
        return { ...req.query, ...req.body, ...req.params }
    };
    res.ok = function (data, msg) {
        res.status(200).json({ code: 200, message: msg ? msg : 'ok', data: data })
    },
    res.error = function (message, code = 500) {
        res.status(code).json({ code: code, error: message })
    }
    next();
});

// =====================< 接口 >=========================

const api = require('./router/api');
app.use('/api', api);


const websocket = require('./router/websocket');
const expressWs = require('express-ws');
expressWs(app);
app.use('/websocket', websocket);

// =====================< 接口 >=========================


/**
 * https://blog.csdn.net/wlqchengzhangji/article/details/122324856
 * 一定要放在最后，不然不执行
 * 当程序抛出错误以后，会自动执行错误处理函数中间件
 * 当服务器发生错误，状态码应该设为500
 * err.stack： 堆栈信息
 * err.message 错误描述
 */
app.use((err, req, res, next) => {
	res.error(err.message);
})

// 启动服务
var host = '127.0.0.1';
var port = 8090;

app.listen(port, host, function () {
  console.log('Mock server is running: \033[40;32m http://' + `${host}:${port}` + '\033[0m');
});