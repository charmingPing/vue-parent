import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import auth from '../utils/auth'

Vue.use(VueRouter)


// 获取原型对象push函数
const originalPush = VueRouter.prototype.push

// 获取原型对象replace函数
const originalReplace = VueRouter.prototype.replace

// 修改原型对象中的push函数
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

// 修改原型对象中的replace函数
VueRouter.prototype.replace = function replace(location) {
  return originalReplace.call(this, location).catch(err => err)
}

const routes = [
  {
    path: '/',
    component: Home,
    children: [
      {
        path: '/',
        name: 'Index',
        meta: { title: '首页', type: 'menu' },
        component: () => import('../views/dashboard/Dashboard.vue')
      },
      {
        path: '/dashboard',
        name: 'Dashboard',
        meta: { title: '首页', type: 'menu' },
        component: () => import('../views/dashboard/Dashboard.vue')
      },
      {
        path: '/menu',
        name: 'Menu',
        meta: { title: '菜单管理', type: 'menu' },
        component: () => import('../views/system/menu/Menu.vue')
      },
      {
        path: '/user',
        name: 'User',
        meta: { title: '用户管理', type: 'menu' },
        component: () => import('../views/system/user/User.vue')
      },
      {
        path: '/user/detail',
        name: 'UserDetail',
        meta: { title: '用户详情', type: 'tab' },
        component: () => import('../views/system/user/UserDetail.vue')
      },
      {
        path: '/dictionary',
        name: 'Dictionary',
        meta: { title: '字典管理', type: 'menu' },
        component: () => import('../views/system/dictionary/Dictionary.vue')
      },
      {
        path: '/account',
        name: 'Account',
        meta: { title: '个人中心', type: 'menu' },
        component: () => import('../views/system/user/UserDetail.vue')
      },
      {
        path: '/language',
        name: 'Language',
        meta: { title: '语言', type: 'menu' },
        component: () => import('../views/setting/language/Language.vue')
      },
      {
        path: '/theme',
        name: 'Theme',
        meta: { title: '主题', type: 'menu' },
        component: () => import('../views/setting/theme/Theme.vue')
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    meta: { title: '登录' },
    component: () => import('../views/login/Login.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

/**
 * [token处理]https://blog.csdn.net/weixin_45745641/article/details/120670092
 * 路由前置守卫：[路由拦截器] -> [统一处理]
 */
router.beforeEach((to, from, next) => {
  document.title = to.meta?.title;

  const token = auth.getToken();
  if (to.path === '/login') {
    if (token) {
      return next('/') // 存在，说明已登录还未过期
    }
    return next()
  }
  if (!token) {
    return next('/login')
  }

  next();
});

export default router
