/**
 * axios封装
 * 请求拦截、相应拦截、错误统一处理
 * 
 * 1.import message from 'ant-design-vue'; 这个写法提示框不弹出，为啥？
 */
import { message } from 'ant-design-vue';
import axios from 'axios';
import QS from 'qs';
import store from '../store/index'
import auth from '../utils/auth'
import router from '../router'


// 环境的切换
if (process.env.NODE_ENV == 'development') {
    axios.defaults.baseURL = 'http://localhost:18081';
} else if (process.env.NODE_ENV == 'debug') {
    axios.defaults.baseURL = '';
} else if (process.env.NODE_ENV == 'production') {
    axios.defaults.baseURL = 'http://api.123dailu.com/';
}

// 请求超时时间
axios.defaults.timeout = 10000;
// axios请求默认不带cookie, 允许带cookies
axios.defaults.withCredentials = true;
// post请求头, 不设置也可以，默认就是application/x-www-form-urlencoded
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

// 请求拦截器
axios.interceptors.request.use(
    config => {
        // 每次发送请求之前判断是否存在token，如果存在，则统一在http请求的header都加上token，不用每次请求都手动添加了
        // 即使本地存在token，也有可能token是过期的，所以在响应拦截器中要对返回状态进行判断
        // const token = store.state.token;
        // const token = window.sessionStorage.getItem('token');
        const token = auth.getToken();
        token && (config.headers.Authorization = 'Bearer ' + token);

        return config;
    },
    error => {
        return Promise.error(error);
    })

// 响应拦截器
axios.interceptors.response.use(
    response => {
        if (response.status === 200) {
            // 判断业务上的错误
            if (response?.data.code !== '00000') {
                message.error(response.data.error);
                // 抛出异常
                return Promise.reject(response);
            }
            // 友好提示
            let method = response.config.method;
            let url = response.config.url;
            if ('put' === method) {
                message.success('修改成功');
            } else if ('delete' === method) {
                message.success('删除成功');
            } else if ('post' === method && url.indexOf('/add') != -1) {
                message.success('添加成功');
            }
            // 判断重定向
            // let fullPath = response.config.baseURL + response.config.url;
            // if (!response.request.responseURL.startWith(fullPath)) {
            //     router.replace({ path: '/login' });
            // }
            return Promise.resolve(response);
        }
    },
    // 服务器状态码不是200的情况    
    error => {
        if (error.response.status) {
            switch (error.response.status) {
                /**
                 * 401: 未登录
                 * 未登录则跳转登录页面，并携带当前页面的路径
                 * 在登录成功后返回当前页面，这一步需要在登录页操作。
                 */
                case 401:
                    router.replace({
                        path: '/login',
                        query: { redirect: router.currentRoute.fullPath }
                    });
                    break;

                /**
                 * 403 token过期
                 * 登录过期对用户进行提示
                 * 清除本地token和清空vuex中token对象
                 * 跳转登录页面  
                 */
                case 403:
                    message.error('登录过期，请重新登录');
                    // 清除token                    
                    localStorage.removeItem('token');
                    store.commit('loginSuccess', null);
                    // 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面
                    setTimeout(() => {
                        router.replace({
                            path: '/login',
                            query: {
                                redirect: router.currentRoute.fullPath
                            }
                        });
                    }, 1000);
                    break;

                /**
                 * 404请求不存在
                 */
                case 404:
                    message.error('网络请求不存在');
                    break;

                /**
                 * 其他错误，直接抛出错误提示  
                 */
                default:
                    message.error(error.response.data.error);
            }
            return Promise.reject(error.response);
        }
    }
);

/** 
 * get方法，对应get请求 
 * @param {String} url [请求的url地址] 
 * @param {Object} params [请求时携带的参数] 
 */
export function get(url, params) {
    return new Promise((resolve, reject) => {
        axios.get(url, {
            params: params
        })
            .then(res => {
                resolve(res.data);
            })
            .catch(err => {
                reject(err.data)
            })
    });
}

/** 
 * post方法，对应post请求 
 * @param {String} url [请求的url地址] 
 * @param {Object} params [请求时携带的参数] 
 */
export function post(url, params) {
    return new Promise((resolve, reject) => {
        axios.post(url, QS.stringify(params))
            .then(res => {
                resolve(res.data);
            })
            .catch(err => {
                reject(err.data)
            })
    });
}

/** 
 * post方法，string方式，后端用 @RequestBody 接收
 * @param {String} url [请求的url地址] 
 * @param {Object} params [请求时携带的参数] 
 */
export function postString(url, params) {
    return new Promise((resolve, reject) => {
        let config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        axios.post(url, JSON.stringify(params), config)
            .then(res => resolve(res.data))
            .catch(err => reject(err.data))
    });
}

/** 
 * 
 * @param {String} url [请求的url地址] 
 * @param {Object} params [请求时携带的参数] 
 */
export function put(url, params) {
    return new Promise((resolve, reject) => {
        axios.put(url, QS.stringify(params))
            .then(res => resolve(res.data))
            .catch(err => reject(err.data))
    });
}

/** 
 * @DeleteMapping("delete")
 * public Result<Void> delete(Long id) {}
 * delete是关键字，换个名字
 * @param {String} url [请求的url地址] 
 * @param {Object} params [请求时携带的参数] 
 */
export function remove(url, params) {
    return new Promise((resolve, reject) => {
        axios.delete(url, { params })
            .then(res => {
                resolve(res.data);
            })
            .catch(err => {
                reject(err.data)
            })
    });
}

/** 
 * 
 * delete是关键字，换个名字
 * @param {String} url [请求的url地址] 
 * @param {Object} params [请求时携带的参数] 
 */
export function removeString(url, params) {
    return new Promise((resolve, reject) => {
        axios.delete(url, params)
            .then(res => {
                resolve(res.data);
            })
            .catch(err => {
                reject(err.data)
            })
    });
}
