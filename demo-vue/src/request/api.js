/**
 * api统一管理
 */
import { get, post, postString, put, remove, removeString } from './http'

/**
 * oauth服务
 */
export const login = p => post('/auth/login', p);
export const getUserById = p => get('/user/getUserById', p);
export const pageUser = p => get('/user/page', p);
export const listUser = p => get('/user/list', p);
export const listMenu = p => get('/menu/list', p);
export const pageMenu = p => get('/menu/page', p);
export const editUser = p => put(`/user/updateById/${p.id}`, p);
export const addUser = p => post('/user/add', p);
export const deleteUser = p => remove('/user/delete', p);
export const deleteUserById = p => remove(`/user/deleteById/${p.id}`, p);
export const deleteUserByIds = p => removeString('/user/deleteByIds', p);