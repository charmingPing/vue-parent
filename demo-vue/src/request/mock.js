/**
 * https://juejin.cn/post/7076801724383494180
 * https://juejin.cn/post/6878208201486139400
 * https://www.jianshu.com/p/84ec8b035479
 * 
 * https://juejin.cn/post/7035489422397145095
 * 
 * 模拟数据
 */
// 写法一
// import { mock, Random } from 'mockjs'
//
//// 获取用户
// mock('/api/user', 'post', {
//     "name": Random.cname(),
//     'age|10-20': 1
// });

// 写法二
import Mock from 'mockjs'

Mock.mock('/api/user', 'post', {
    "name": '@cname',
    'age|10-20': 1
});




