import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 全局引入mock, 但是这种方式不走network
// import './request/mock'

/**
 * 全局加载，babel.config.js中plugins注释掉ant
 * 挂载到vue中
 */
import Antd from 'ant-design-vue/es';
import 'ant-design-vue/dist/antd.css';

/**
 * 这个iconfont.js就是从iconfont.cn网站上下载后解压的JS文件
 */
import { Icon } from 'ant-design-vue';
import iconFront from './assets/icon/iconfont.js';
const myicon = Icon.createFromIconfontCN({
  scriptUrl: iconFront
});
Vue.component('my-icon', myicon);

// 最后挂到vue中
Vue.use(Antd);


// 按需加载，babel.config.js中plugins放开ant
// import { Layout, Menu, Icon } from 'ant-design-vue';
// Vue.use(Layout);
// Vue.use(Menu);
// Vue.use(Icon);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
