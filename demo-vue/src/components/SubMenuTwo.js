import { Menu } from 'ant-design-vue';
const SubMenu = {
  template: `
      <a-sub-menu :key="menuInfo.key" v-bind="$props" v-on="$listeners">
        <span slot="title">
          <a-icon :type="menuInfo.icon" /><span>{{ menuInfo.title }}</span>
        </span>
        <template v-for="item in menuInfo.children">
          <a-menu-item v-if="!(item.children && item.children.length)" :key="item.key">
            <a-icon :type="item.icon" />
            <span>{{ item.title }}</span>
          </a-menu-item>
          <a-menu-item-group v-else-if="item.type === 2" :key="item.key">
            <template slot="title"> <a-icon :type="item.icon" /><span style="margin-left: 5px;vertical-align: middle">{{ item.title }}</span> </template>
            <template v-for="child in item.children">
              <!-- Menu.ItemGroup 的子元素必须是 MenuItem. -->
              <a-menu-item :key="child.key">
                <a-icon :type="child.icon" />
                <span>{{ child.title }}</span>
              </a-menu-item>
            </template>
          </a-menu-item-group>
          <sub-menu v-else :key="item.key" :menu-info="item" />
        </template>
      </a-sub-menu>
    `,
  name: 'SubMenu',
  isSubMenu: true,
  props: {
    ...Menu.SubMenu.props,
    menuInfo: {
      type: Object,
      default: () => ({}),
    }
  }
}

export default SubMenu