import Vue from 'vue'
import Storage from 'vue-ls';

const options = {
  namespace: 'vuejs__', // key键前缀
  name: 'ls', // 命名Vue变量.[ls]或this.[$ls],
  storage: 'local', // 存储名称: session, local, memory
};

Vue.use(Storage, options);


/**
 * https://juejin.cn/post/7072236499231572005
 * 不同的应用都叫token, 会发生什么问题
 */
function getToken() {
    return Vue.ls.get('token');
}

function setToken(token) {
    // return Vue.ls.set('token', token, 60 * 60 * 1000); //有效1小时
    return Vue.ls.set('token', token);
}

function clearToken() {
    return Vue.ls.remove('token');
}

function getUser() {
    return Vue.ls.get('user');
}

function setUser(user) {
    return Vue.ls.set('user', user, 60 * 60 * 1000); //有效1小时
}

function clearUser() {
    return Vue.ls.remove('user');
}

function logout() {
    clearToken();
    clearUser();
}


export default {
    getToken,
    setToken,
    clearToken,
    getUser,
    setUser,
    clearUser,
    logout
}